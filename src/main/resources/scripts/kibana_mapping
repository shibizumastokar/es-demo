# Inserting a dcoument in to index

# -> Explain the mapping parameter like dynamic, copy_to, coerce, properties, norms, format, null_value,
     fields

PUT /customer/_doc/1
{
  "name": "Abhishek Chauhan",
  "signup_date": "2018/05/06",
  "reliability_index": [
    10,
    20
  ]
}

# Analysing the dynamic mapping created by elastic for above index
GET /customer

# Deleting the index
DELETE /customer

# Creating over own custom mapping with index
PUT /customer
{
  "mappings": {
    "_doc": {
      "dynamic": false,
      "properties": {
        "first_name": {
          "type": "text",
          "copy_to": "full_name",
          "fields": {
            "keyword": {
              "type": "keyword"
            },
            "raw": {
              "type": "keyword"
            }
          }
        },
        "last_name": {
          "type": "text",
          "copy_to": "full_name",
          "fields": {
            "keyword": {
              "type": "keyword"
            },
            "raw": {
              "type": "keyword"
            }
          }
        },
        "age": {
          "type": "integer"
        },
        "dob": {
          "type": "date",
          "format": "yyyy-MM-dd||epoch_millis"
        },
        "active": {
          "type": "boolean"
        },
        "reliability_score": {
          "type": "integer_range"
        },
        "location": {
          "type": "geo_point"
        },
        "ip_adr": {
          "type": "ip"
        },
        "relationship_maturity": {
          "type": "integer",
          "null_value": 0
        },
        "description": {
          "dynamic": true,
          "type": "object"
        },
        "tags": {
          "type": "text"
        }
      }
    }
  }
}

# Analysing custom mapping created by us
GET /customer

# inserting document into newly created mapping
PUT /customer/_doc/1
{
  "first_name": "Akshay",
  "last_name": "Lamba",
  "age": 25,
  "dob": "1992-05-28",
  "active": true,
  "reliability_score": {
    "gte": 4,
    "lte": 9
  },
  "location": {
    "lat": 40.72,
    "lon": -73.97
  },
  "ip_adr": "120.9.32.3",
  "description": {
    "loans": 2,
    "defaulted": 0
  },
  "tags": [
    "senior",
    "premium"
  ],
  "interest_rate": 5
}

# lets see the result
GET /customer/_doc/1

# lets search on interest value
GET /customer/_doc/_search
{
  "query": {
    "term": {
      "interest_rate": {
        "value": 5
      }
    }
  }
}

# Modifying mapping for an index
PUT /customer/_doc/_mapping
{
  "properties": {
    "interest_rate": {
      "type": "integer"
    }
  }
}

# Updating over indexes
POST /customer/_update_by_query?conflicts=proceed

# lets search on interest value now
GET /customer/_doc/_search
{
  "query": {
    "term": {
      "interest_rate": {
        "value": 5
      }
    }
  }
}
