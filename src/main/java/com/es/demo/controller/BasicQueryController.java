package com.es.demo.controller;

import com.es.demo.model.QueryRequest;
import com.es.demo.service.impl.BasicQueryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/es-demo/queries")
public class BasicQueryController {

  @Autowired
  private BasicQueryServiceImpl basicQueryService;

  @RequestMapping(value = "/match-all-query")
  public List matchAllQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.matchAllQuery(queryRequest);
  }

  @RequestMapping(value = "/term-query")
  public List termQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.termQuery(queryRequest);
  }

  @RequestMapping(value = "/terms-query")
  public List termsQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.termsQuery(queryRequest);
  }

  @RequestMapping(value = "/ids-query")
  public List idsQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.idsQuery(queryRequest);
  }

  @RequestMapping(value = "/range-query")
  public List rangeQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.rangeQuery(queryRequest);
  }

  @RequestMapping(value = "/exists-query")
  public List existsQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.existsQuery(queryRequest);
  }

  @RequestMapping(value = "/prefix-query")
  public List prefixQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.prefixQuery(queryRequest);
  }

  @RequestMapping(value = "/wildcard-query")
  public List wildCardQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.wildCardQuery(queryRequest);
  }

  @RequestMapping(value = "/match-query-with-or")
  public List matchQueryWithOr(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.matchQueryWithOr(queryRequest);
  }

  @RequestMapping(value = "/match-query-with-and")
  public List matchQueryWithAnd(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.matchQueryWithAnd(queryRequest);
  }

  @RequestMapping(value = "/match-phrase-query")
  public List matchPhraseQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.matchPhraseQuery(queryRequest);
  }

  @RequestMapping(value = "/multi-match-query")
  public List multiMatchQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.multiMatchQuery(queryRequest);
  }

  @RequestMapping(value = "/bool-query")
  public List boolQuery(@RequestBody QueryRequest queryRequest) throws Exception {
    return basicQueryService.boolQuery(queryRequest);
  }
}
