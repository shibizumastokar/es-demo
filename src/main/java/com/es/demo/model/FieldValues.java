package com.es.demo.model;

public interface FieldValues {

  String ACTIVE = "active";
  String DEFAULTING_SLAB_RAW = "defaulting_slab.raw";
  String JOINING_DATE = "joining_date";
  String BARRED = "barred_due_to_reason";
  String FIRST_NAME = "first_name";
  String FIRST_NAME_RAW = "first_name.raw";
  String DESCRIPTION = "description";
}
