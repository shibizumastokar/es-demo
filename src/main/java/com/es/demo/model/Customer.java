package com.es.demo.model;

public class Customer {
  private String id;
  private String firstName;
  private String lastName;
  private Boolean active;
  private String defaultingSlab;
  private String joiningDate;
  private String description;
  private String barredDueToReason;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getDefaultingSlab() {
    return defaultingSlab;
  }

  public void setDefaultingSlab(String defaultingSlab) {
    this.defaultingSlab = defaultingSlab;
  }

  public String getJoiningDate() {
    return joiningDate;
  }

  public void setJoiningDate(String joiningDate) {
    this.joiningDate = joiningDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getBarredDueToReason() {
    return barredDueToReason;
  }

  public void setBarredDueToReason(String barredDueToReason) {
    this.barredDueToReason = barredDueToReason;
  }
}
