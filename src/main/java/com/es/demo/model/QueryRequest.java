package com.es.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryRequest {

  private String searchKeyword;
  private Boolean active;
  private List<String> defaultingSlabs;
  private String id;
  private String fromDate;
  private String toDate;
  private String fieldExists;

  public String getSearchKeyword() {
    return searchKeyword;
  }

  public void setSearchKeyword(String searchKeyword) {
    this.searchKeyword = searchKeyword;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public List<String> getDefaultingSlabs() {
    return defaultingSlabs;
  }

  public void setDefaultingSlabs(List<String> defaultingSlabs) {
    this.defaultingSlabs = defaultingSlabs;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFromDate() {
    return fromDate;
  }

  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  public String getToDate() {
    return toDate;
  }

  public void setToDate(String toDate) {
    this.toDate = toDate;
  }

  public String getFieldExists() {
    return fieldExists;
  }

  public void setFieldExists(String fieldExists) {
    this.fieldExists = fieldExists;
  }
}
