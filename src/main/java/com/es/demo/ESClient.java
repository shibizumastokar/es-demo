package com.es.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
public class ESClient {

  @Autowired
  private ESDemoConfig esDemoConfig;

  private TransportClient client;

  ObjectMapper objectMapper;

  @PostConstruct
  private void initialize() {
    client = getElasticClient();
    objectMapper = new ObjectMapper();
  }

  public ESDemoConfig getEsDemoConfig() {
    return esDemoConfig;
  }

  public TransportClient getInstant() {
    return client;
  }

  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public void closeClient(TransportClient client) {
    if (client != null) {
      client.close();
    }
  }

  private TransportClient getElasticClient() {
    try {
      Settings setting = Settings.builder()
          .put("cluster.name", esDemoConfig.getCluster())
          .put("client.transport.sniff", Boolean.TRUE).build();

      client = new PreBuiltTransportClient(setting)
          .addTransportAddress(new TransportAddress(InetAddress.getByName(esDemoConfig.getHost()),
              Integer.valueOf(esDemoConfig.getPort())));
    } catch (UnknownHostException ex) {
      System.out.println("Error :)");
    }
    return client;
  }
}
