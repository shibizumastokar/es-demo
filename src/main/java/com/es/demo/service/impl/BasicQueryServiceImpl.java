package com.es.demo.service.impl;

import com.es.demo.model.Customer;
import com.es.demo.model.QueryRequest;
import com.es.demo.service.BasicQueryExecutor;
import com.es.demo.service.builder.BasicQueryBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

import static com.es.demo.model.FieldValues.FIRST_NAME_RAW;

@Service
public class BasicQueryServiceImpl {

  @Autowired
  private BasicQueryBuilder basicQueryBuilder;

  @Autowired
  private BasicQueryExecutor basicQueryExecutor;

  public List matchAllQuery(QueryRequest queryRequest) throws Exception {
    LinkedHashMap<String, SortOrder> sort = new LinkedHashMap<>();
    sort.put(FIRST_NAME_RAW, SortOrder.ASC);
    return basicQueryExecutor.execute(basicQueryBuilder.buildMatchAllQuery(queryRequest), Customer.class, sort);
  }

  public List termQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildTermQuery(queryRequest), Customer.class, null);
  }

  public List termsQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildTermsQuery(queryRequest), Customer.class, null);
  }

  public List idsQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildIdsQuery(queryRequest), Customer.class, null);
  }

  public List rangeQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildRangeQuery(queryRequest), Customer.class, null);
  }

  public List existsQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildExistsQuery(queryRequest), Customer.class, null);
  }

  public List prefixQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildPrefixQuery(queryRequest), Customer.class, null);
  }

  public List wildCardQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildWildCardQuery(queryRequest), Customer.class, null);
  }

  public List matchQueryWithOr(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildMatchQueryWithOr(queryRequest), Customer.class, null);
  }

  public List matchQueryWithAnd(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildMatchQueryWithAnd(queryRequest), Customer.class, null);
  }

  public List matchPhraseQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildMatchPhraseQuery(queryRequest), Customer.class, null);
  }

  public List multiMatchQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildMutliMatchQuery(queryRequest), Customer.class, null);
  }

  public List boolQuery(QueryRequest queryRequest) throws Exception {
    return basicQueryExecutor.execute(basicQueryBuilder.buildBoolQuery(queryRequest), Customer.class, null);
  }
}
