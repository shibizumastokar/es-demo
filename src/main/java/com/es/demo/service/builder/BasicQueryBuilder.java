package com.es.demo.service.builder;

import com.es.demo.model.QueryRequest;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import static com.es.demo.model.FieldValues.*;

@Service
public class BasicQueryBuilder {

  public QueryBuilder buildMatchAllQuery(QueryRequest queryRequest) {
    return QueryBuilders.matchAllQuery();
  }

  public QueryBuilder buildTermQuery(QueryRequest queryRequest) {
    return queryRequest.getActive() != null ?
        QueryBuilders.termQuery(ACTIVE, queryRequest.getActive()) : buildMatchAllQuery(queryRequest);
  }

  public QueryBuilder buildTermsQuery(QueryRequest queryRequest) {
    return CollectionUtils.isEmpty(queryRequest.getDefaultingSlabs()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.termsQuery(DEFAULTING_SLAB_RAW, queryRequest.getDefaultingSlabs());
  }

  public QueryBuilder buildIdsQuery(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getId()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.idsQuery().addIds("1");
  }

  public QueryBuilder buildRangeQuery(QueryRequest queryRequest) {
    return !(queryRequest.getFromDate() != null && queryRequest.getToDate() != null) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.rangeQuery(JOINING_DATE)
        .from(queryRequest.getFromDate(), true).to(queryRequest.getToDate(), true);
  }

  public QueryBuilder buildExistsQuery(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getFieldExists()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.existsQuery(BARRED);
  }

  public QueryBuilder buildPrefixQuery(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getSearchKeyword()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.prefixQuery(DEFAULTING_SLAB_RAW, queryRequest.getSearchKeyword());
  }

  public QueryBuilder buildWildCardQuery(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getSearchKeyword()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.wildcardQuery(DEFAULTING_SLAB_RAW, queryRequest.getSearchKeyword());
  }

  public QueryBuilder buildMatchQueryWithOr(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getSearchKeyword()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.matchQuery(FIRST_NAME, queryRequest.getSearchKeyword());
  }

  public QueryBuilder buildMatchQueryWithAnd(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getSearchKeyword()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.matchQuery(DESCRIPTION, queryRequest.getSearchKeyword())
        .operator(Operator.AND);
  }

  public QueryBuilder buildMatchPhraseQuery(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getSearchKeyword()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.matchPhraseQuery(DESCRIPTION, queryRequest.getSearchKeyword());
  }

  public QueryBuilder buildMutliMatchQuery(QueryRequest queryRequest) {
    return StringUtils.isEmpty(queryRequest.getSearchKeyword()) ?
        buildMatchAllQuery(queryRequest) : QueryBuilders.multiMatchQuery(queryRequest.getSearchKeyword(), DESCRIPTION, BARRED);
  }

  public QueryBuilder buildBoolQuery(QueryRequest queryRequest) {
    return QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery(DESCRIPTION, queryRequest.getSearchKeyword()))
        .mustNot(QueryBuilders.termsQuery(DEFAULTING_SLAB_RAW, queryRequest.getDefaultingSlabs()))
        .should(QueryBuilders.matchQuery(DESCRIPTION, queryRequest.getFieldExists()).boost(5.0f))
        .filter(QueryBuilders.rangeQuery(JOINING_DATE)
            .from(queryRequest.getFromDate(), true)
            .to(queryRequest.getToDate(), true));
  }
}
