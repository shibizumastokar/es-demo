package com.es.demo.service;

import com.es.demo.ESClient;
import com.es.demo.model.Customer;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class BasicQueryExecutor<T> {

  @Autowired
  private ESClient esClient;

  public List execute(QueryBuilder query, Class<T> cls, LinkedHashMap<String,SortOrder> sort) throws Exception {
    SearchRequestBuilder requestBuilder = esClient.getInstant()
        .prepareSearch(esClient.getEsDemoConfig().getIndex())
        .setQuery(query)
        .setExplain(true);
    if(!CollectionUtils.isEmpty(sort)) {
      sort.forEach(requestBuilder::addSort);
    }
    return prepareResult(requestBuilder.execute().actionGet().toString(), cls);
  }

  private List prepareResult(String response, Class<T> cls) throws Exception {
    if (cls == Customer.class) {
      return prepareResultByCustomer((List<Map>) ((Map) esClient.getObjectMapper().readValue(response, Map.class).get("hits")).get("hits"));
    }
    return null;
  }

  private List<Customer> prepareResultByCustomer(List<Map> actualHits) throws Exception {
    List<Customer> customers = new ArrayList<>(actualHits.size());
    for (Map map : actualHits) {
      Customer customer = new Customer();
      Map<String, Object> source = (Map) map.get("_source");
      customer.setId((String) map.get("_id"));
      customer.setFirstName((String) source.get("first_name"));
      customer.setLastName((String) source.get("last_name"));
      customer.setActive((Boolean) source.get("active"));
      customer.setDefaultingSlab((String) source.get("defaulting_slab"));
      customer.setBarredDueToReason((String) source.get("barred_due_to_reason"));
      customer.setJoiningDate((String) source.get("joining_date"));
      customer.setDescription((String) source.get("description"));
      customers.add(customer);
    }
    return customers;
  }
}
