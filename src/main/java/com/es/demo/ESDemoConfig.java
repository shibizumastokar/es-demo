package com.es.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ESDemoConfig {

  @Value("${host}")
  private String host;

  @Value("${port}")
  private int port;

  @Value("${cluster}")
  private String cluster;

  @Value("${index}")
  private String index;

  public String getIndex() {
    return index;
  }

  public String getHost() {
    return host;
  }

  public int getPort() {
    return port;
  }

  public String getCluster() {
    return cluster;
  }
}
